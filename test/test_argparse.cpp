#include <iostream>

#include <cvx/util/misc/arg_parser.hpp>
#include <cvx/util/misc/strings.hpp>

using namespace std ;
using namespace cvx::util ;

struct BBox {
    bool empty_ = true ;
    float min_x_, min_y_, max_x_, max_y_ ;

    BBox() = default ;
    BBox(float minx, float miny, float maxx, float maxy):
        min_x_(minx), min_y_(miny), max_x_(maxx), max_y_(maxy), empty_(false) {}

    friend std::istream &operator >> ( std::istream &strm, BBox &box ) {
        strm >>box.min_x_ >> box.min_y_ >> box.max_x_ >> box.max_y_ ;
        box.empty_ = false ;
        return strm ;
    }
};

int main(int argc, const char *argv[]) {

    ArgumentParser args ;

    BBox bbox ;
    float f = 2.0;
    int val ;
    std::vector<std::string> files ;
    std::vector<std::string> items ;
    int pos ;
    bool print_help = false ;
    args.addOption("-h|--help", print_help, true).setMaxArgs(0).setDescription("print this help message") ;
    args.addOption("-f|--flag", f, (float)4.0).setMinArgs(0).setDescription("first flag").setName("[<arg>]") ;
    args.addOption("-b|--bbox", bbox).setMinArgs(4).setDescription("bounding box").setName("<minx> <miny> <maxx> <maxy>") ;
    args.addOption("--test", items).setMinArgs(2).setMaxArgs(-1).setDescription("second flag").setName("<arg1> <arg2> [ ... <argN>]") ;
    args.addOption("--custom", [&](istream &strm) {
        strm >> val ;
        return true ;
    }).setDescription("custom flag") ;
    args.addPositional(pos) ;
    args.addPositional(files).setMaxArgs(-1) ;

    if ( !args.parse(argc, argv) || print_help ) {
        cout << "Usage: prog [options] position files" << endl ;
        cout << "Options:" << endl ;
        args.printOptions(std::cout) ;
    }

}
