#ifndef __ARG_PARSER_HPP__
#define __ARG_PARSER_HPP__

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <istream>
#include <iterator>
#include <sstream>


namespace cvx { namespace util {

typedef std::function<bool(std::istream &)> ValueWriter ;

// Minimal command line argument parser
// Add options using addOption and addPositional functions, then call parse function
// The class does not act as a container of parsed values. Instead the values are stored in user variables which have to be valid when parse is called.
// Reading of arguments into values is performed internally be means of stream >> operations and therefore any variable type that supports this operator
// can be used. To use custom const values (i.e. values set by default when no argument passed for an option) one must also implement the associated stream
// write operator i.e. <<. Internally the const value is first written to a stringstream which is the passed to the value reader.

class ArgumentParser {

    class Value ;
    class ConstValue ;

public:

    ArgumentParser() {}

    class Option ;

    // add option that stores the arguments in the reference value
    // the template parameter can be any type defining the istream >> operator
    // flags is a list of acceptable flags (delimited by '|' or ',')
    // It is the user to set a default value to the variable before calling the parser. This will not be touched
    // if the option is not given on the command line
    template<class T>
    Option &addOption(const std::string &flags, T &value) {
        options_.emplace_back(flags, std::make_shared<ValueHolder<T>>(value)) ;
        return options_.back();
    }

    // same as above but also taking a constant value to be set if the option is present and has no arguments
    template<class T, class S>
    Option &addOption(const std::string &flags, T &value, const S &c) {
        options_.emplace_back(flags,
                              std::make_shared<ValueHolder<T>>(value),
                              std::make_shared<ConstValueHolder<S>>(c)) ;
        return options_.back();
    }


    // add option that stores the arguments to a vector of values (i.e. to accept multiple args)
    // the template parameter can be any type defining the istream >> operator
    // flags is a list of acceptable flags (delimited by '|' or ',')
    // No const value is specified here since the presence or not of the option may be asserted by the variable being empty
    template<class T>
    Option &addOption(const std::string &flags, std::vector<T> &value) {
        options_.emplace_back(flags, std::make_shared<ValueListHolder<T>>(value)) ;
        return options_.back() ;
    }

    // Use this for defining a custom writter
    // The writer is a lambda that should parse the value and store it to a captured variable somewhere
    // flags is a list of acceptable flags (delimited by '|' or ',')

    Option &addOption(const std::string &flags, ValueWriter writer) {
        options_.emplace_back(flags, std::make_shared<ValueAdapter>(writer)) ;
        return options_.back() ;
    }

    template<class T, class S>
    Option &addOption(const std::string &flags, ValueWriter writer, const S &c) {
        options_.emplace_back(flags,
                              std::make_shared<ValueAdapter>(writer),
                              std::make_shared<ConstValueHolder<S>>(c)) ;
        return options_.back() ;
    }

    // Positional options
    template<class T>
    Option &addPositional(T &value) {
        positional_.emplace_back(std::make_shared<ValueHolder<T>>(value)) ;
        return positional_.back();
    }

    template<class T>
    Option &addPositional(std::vector<T> &value) {
        positional_.emplace_back(std::make_shared<ValueListHolder<T>>(value)) ;
        return positional_.back();
    }

    Option &addPositional(ValueWriter writer) {
        positional_.emplace_back(std::make_shared<ValueAdapter>(writer)) ;
        return positional_.back();
    }

    // parse the command line and return success or failure
    bool parse(int argc, const char *argv[]) ;

    void printOptions(std::ostream &strm, uint line_length = 80, uint min_description_width = 40) ;

    class Option {
    public:

       // Set minimum args expected ( default 1 )
       Option &setMinArgs(int minargs) {
           min_args_ = minargs ;
           if ( max_args_ >= 0 )
               max_args_ = std::max(min_args_, max_args_) ;
           return *this ;
       }

       // Set maximum args expected ( default 1 ). set to -1 for infinite
       Option &setMaxArgs(int maxargs) {
           max_args_ = maxargs ;
           if ( maxargs >= 0 )
               min_args_ = std::min(min_args_, maxargs) ;
           return *this ;
       }

       // Set option as required (not checked for positional arguments)
       Option &required(bool req = true) {
           is_required_ = req ;
           return *this ;
       }

       // Set description paragraph to be written on help output
       Option &setDescription(const std::string &desc) {
           description_ = desc ;
           return *this ;
       }

       // Set variable name to be written on help output (default is <arg>)
       // We let the user provide a concise and meaningful semantic name (e.g. including default values)
       // instead of trying to make this automatically
       Option &setName(const std::string &name) {
           name_ = name ;
           return *this ;
       }

       Option(const std::string& flags, std::shared_ptr<Value> val, std::shared_ptr<ConstValue> cval = nullptr ) ;
       Option(std::shared_ptr<Value> val) ;

    protected:

          friend class ArgumentParser ;
          bool matches(const std::string &arg) const ;

          std::string formatOptionFlags() const ;
          void printHelp(std::ostream &strm) const;
          void printDescription(std::ostream &strm, uint first_col_width, uint line_length) const;

          std::vector<std::string> flags_;  // list of flags that this option accepts
          std::string description_ ;        // description of this arg
          std::string short_flag_ ;         // short flag selected from the flags
          std::string name_ ;               // the arg name that will appear after the flag in the help printout
          bool is_required_, is_positional_ ;
          std::shared_ptr<Value> value_ ;
          std::shared_ptr<ConstValue> const_ ; // constant value to write for optional fields
          bool matched_ ;
          int min_args_, max_args_ ;        // minimum and maximum args acceptable
    };

private:

    class Value {
    public:
        virtual bool read(std::istream &s) = 0 ;
    };

    class ConstValue {
    public:
        virtual void write(std::ostream &s) = 0 ;
    };


    template <class T>
    class ValueHolder: public Value {
    public:
        ValueHolder(T &t): val_(t) {}
        bool read(std::istream &strm)  {
            strm >> val_ ;
            return (bool)strm ;
        }
        bool write(std::ostream &strm)  {
            strm << val_ ;
            return (bool)strm ;
        }

    private:
        T &val_ ;
    };

    template <class T>
    class ConstValueHolder: public ConstValue {
    public:
        ConstValueHolder(const T &t): val_(t) {}

        void write(std::ostream &strm)  {
            strm << val_ ;
        }

    private:
        T val_ ;
    };


    template <class T>
    class ValueListHolder: public Value {
    public:
        ValueListHolder(std::vector<T> &t): list_(t) {}
        bool read(std::istream &stream)  {
            std::istream_iterator< T > it { stream }, end;
            std::copy( it, end, std::back_inserter(list_) );
            return stream.eof() ;
        }


    private:
        std::vector<T> &list_ ;
    };


    class ValueAdapter: public Value {
    public:
        ValueAdapter(ValueWriter writer): writer_(writer) {}

        bool read(std::istream &s) {
            return writer_(s) ;
        }

    private:
        ValueWriter writer_ ;
    };


private:
    typedef std::vector<Option> Container ;

    bool consumeArg(const Container::iterator &match, int argc, const char *argv[], uint &c);
    Container::iterator findMatchingArg(const std::string &arg) ;
    bool checkRequired() ;
    uint getOptimalColumnWidth(uint line_length, uint min_description_length);

    Container options_, positional_ ;
    std::string options_caption_, positional_caption_, description_ ;
    std::string prog_ ;
    std::string usage_ ;

};

template<>
bool ArgumentParser::ValueHolder<bool>::read(std::istream &strm) {
    std::string s ;
    strm >> s ;
    if ( s == "true" || s == "1" ) {
        val_ = true ;
        return true ;
    }
    else if ( s == "false" || s == "0" ) {
        val_ = false ;
        return true ;
    }
    else return false ;
}

}}
#endif
